### Building and running your application

When you're ready, start your application by running:
`docker compose up --build`.

Add frontend/.env for backend. Example:
`VITE_API_BACKEND=http://localhost:5001`